<?php
    include "class.php";
?>
<html>
    <head>
        <title>object oriented php</title>
    </head>
    <body>
        <p>
        <?php
        $text = 'hello world';
            echo "$text and the universe";
            echo '<br>';
            $msg = new message();
            //echo $msg->text;
            $msg->show();
            echo message::$count;
            $msg1 = new message("a new text");
            $msg1->show();
            echo message::$count;
            $msg2 = new message();
            $msg2->show();
            echo message::$count;
            $msg3 = new redmessage("a red message");
            $msg3->show();
            $msg4 = new coloredmessage();
            $msg4->color = 'green';
            $msg4->show();
            echo '<br>';
            showobject($msg1);
            var_dump($msg4);
        ?>
        </p>
    </body>
</html>